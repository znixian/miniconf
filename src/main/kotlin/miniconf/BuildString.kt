package miniconf

import miniconf.exec.VariableSlot

class BuildString() {
    private val parts: MutableList<Part> = ArrayList()

    constructor(str: String) : this(Part.Static(str))

    constructor(part: Part) : this() {
        add(part)
    }

    constructor(fi: RelativeFile) : this(Part.File(fi))

    sealed class Part {
        class Static(val value: String) : Part()
        class ShellEval(val command: CommandBuilder) : Part()
        class PrintCommand(val command: CommandBuilder) : Part()
        object Space : Part()
        class File(val file: RelativeFile) : Part()
        class Variable(val slot: VariableSlot) : Part()
    }

    fun add(str: String) {
        parts += Part.Static(str)
    }

    fun addBashArgument(str: String, space: Boolean = true) {
        if (space)
            pad()

        add(escapeBash(str, QuotingMode.OPT_QUOTE))
    }

    override fun toString(): String = "BuildString{" + toString(Context.Human) + "}"

    fun toString(context: Context): String {
        val sb = StringBuilder()

        for (i in 0 until parts.size) {
            val stringPart = when (val part = parts[i]) {
                is Part.Static -> when (context) {
                    is Context.Configure -> escapeBash(part.value, QuotingMode.HEREDOC)
                    is Context.InlineShell -> escapeBash(part.value, QuotingMode.OPT_QUOTE)
                    else -> part.value
                }
                is Part.ShellEval -> when (context) {
                    is Context.Human -> "eval(${part.command})"
                    is Context.Configure, Context.InlineShell -> "\$(${part.command.toShell()})"
                    is Context.Ninja -> processToString(part.command.toNinja(context))
                }
                is Part.Space -> if (i > 0 && i < parts.size - 1 && parts[i - 1] !is Part.Space) " " else ""
                is Part.File -> when (part.file.relative) {
                    FileBase.SOURCE -> context.srcDir + part.file.path
                    FileBase.OBJECTS -> context.objDir + part.file.path
                    FileBase.BUILD -> context.buildDir + part.file.path
                }
                is Part.Variable -> when (context) {
                    Context.Human -> part.slot.initialName
                    is Context.Configure, Context.InlineShell -> "\${${part.slot.mangledName}}"
                    is Context.Ninja -> TODO()
                }
                is Part.PrintCommand -> part.command.toShell()
            }
            sb.append(stringPart)
        }

        return sb.toString()
    }

    fun add(str: BuildString) {
        parts += str.parts
    }

    fun add(part: Part) {
        when (part) {
            is Part.Space -> if (parts.isNotEmpty() && parts.last() is Part.Space) return
        }

        parts += part
    }

    fun pad() {
        add(Part.Space)
    }

    fun add(fi: RelativeFile) {
        add(Part.File(fi))
    }

    sealed class Context {
        abstract val srcDir: String
        abstract val objDir: String
        abstract val buildDir: String

        object Human : Context() {
            override val srcDir: String get() = "<srcdir>/"
            override val objDir: String get() = "<objdir>/"
            override val buildDir: String get() = "<build>/"
        }

        class Ninja(
                override val srcDir: String,
                override val objDir: String,
                override val buildDir: String
        ) : Context()

        object Configure : Context() {
            override val srcDir: String get() = "\$SRC_DIR/"
            override val objDir: String get() = "\$OBJ_DIR/"
            override val buildDir: String get() = "\$BUILD_DIR/"
        }

        object InlineShell : Context() {
            override val srcDir: String get() = throw Exception("Unsupported")
            override val objDir: String get() = throw Exception("Unsupported")
            override val buildDir: String get() = throw Exception("Unsupported")
        }
    }

    enum class QuotingMode {
        FORCE_QUOTE,
        OPT_QUOTE,
        HEREDOC,
    }

    companion object {
        private val escapeTriggerChars: List<Char> = listOf(
                '|', '&', ';', '<', '>', '(', ')', '$', '`', '\\', '"',
                '\'', ' ', '\t', '\n', '*', '?', '[', '#', '~', '=', '%'
        )

        private fun escape(sb: StringBuilder, char: Char) {
            var idx = 0
            while (idx < sb.length) {
                if (sb[idx] == char) {
                    sb.insert(idx, '\\')
                    idx++
                }

                idx++
            }
        }

        private fun escapeBash(str: String, quoting: QuotingMode): String {
            // TODO properly handle quoting
            var needsEscaping = false
            for (tok in escapeTriggerChars) {
                if (!str.contains(tok))
                    continue

                needsEscaping = true
                break
            }

            val sb = StringBuilder(str)

            val shouldQuote = when (quoting) {
                QuotingMode.FORCE_QUOTE -> true
                QuotingMode.OPT_QUOTE -> needsEscaping
                QuotingMode.HEREDOC -> false
            }

            escape(sb, '\\')
            escape(sb, '$')
            escape(sb, '`')

            when (quoting) {
                QuotingMode.HEREDOC -> {
                    // See https://www.gnu.org/software/bash/manual/bash.html#Here-Documents
                    // Quotes are maintained in heredocs
                }
                else -> {
                    // See http://pubs.opengroup.org/onlinepubs/7908799/xcu/chap2.html#tag_001_002_001
                    escape(sb, '"')
                }
            }

            if (shouldQuote) {
                sb.insert(0, '"')
                sb.append('"')
            }

            return sb.toString()
        }
    }
}
