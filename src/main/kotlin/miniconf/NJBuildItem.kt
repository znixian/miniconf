package miniconf

data class NJBuildItem(val rule: NJBuildRule, val outputs: List<RelativeFile>, val inputs: List<RelativeFile>)

data class NJBuildRule(val properties: Map<BuildString, Any>)