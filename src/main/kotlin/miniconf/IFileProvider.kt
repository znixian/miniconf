package miniconf

import miniconf.exec.commands.Decl

interface IFileProvider {
    val id: String

    fun appliesTo(file: RelativeFile): Boolean

    fun generate(file: RelativeFile, flags: List<String>, decl: Decl): NJBuildItem
}