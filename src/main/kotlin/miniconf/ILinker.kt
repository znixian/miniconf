package miniconf

import miniconf.exec.commands.Decl

interface ILinker {
    fun supports(id: String): Boolean

    fun generate(output: String, inputs: List<RelativeFile>, flags: List<String>, decl: Decl): NJBuildItem
}
