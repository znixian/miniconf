package miniconf

import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import kotlin.system.exitProcess


fun streamToString(stream: InputStream): String =
        BufferedReader(InputStreamReader(stream, Charsets.UTF_8)).lines()
                .reduce { a, b -> "$a\n$b" }.orElse("")

fun processToString(args: List<String>): String {
    val process = ProcessBuilder(args).start()
    val returnCode = process.waitFor()

    if (returnCode != 0) {
        System.err.println("Could not run pkg-config - args: $args")
        System.err.println(streamToString(process.errorStream))
        System.err.println()
        System.err.println(streamToString(process.inputStream))
        exitProcess(1)
    }

    return streamToString(process.inputStream)
}

data class RelativeFile(val relative: FileBase, val path: String) {
    val name: String = path.substring(path.lastIndexOf('/') + 1)
    val nameWithoutExtension: String get() = name.substring(0, name.lastIndexOf('.'))
}

enum class FileBase {
    SOURCE,
    BUILD,
    OBJECTS,
}