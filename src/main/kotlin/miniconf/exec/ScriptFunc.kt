package miniconf.exec

import miniconf.IFunc
import miniconf.exec.commands.CommandValue

class ScriptFunc(val name: String?, val args: List<String>, parentScope: Scope?) : Value(), IFunc {
    val items: MutableList<CommandValue> = ArrayList()
    val isGlobal: Boolean = name == null
    val scope: Scope = Scope(parentScope)

    val mangledName: String get() = "mcf_${name!!}"

    init {
        for (arg in args) {
            scope[arg] = VariableSlot(scope, arg)
        }
    }

    override fun toShell(sb: StringBuilder) {
        if (!isGlobal) {
            sb.append("function ")
            sb.append(mangledName)
            sb.append(" {\n")
        }
        for (i in 0 until args.size) {
            sb.append(scope[args[i]]!!.mangledName)
            sb.append("=\"\$")
            sb.append(i + 1)
            sb.append("\"\n")
        }
        for (item in items) {
            item.toShell(sb)
        }
        if (!isGlobal) {
            sb.append("}\n")
        }
    }

    override fun invokeAtShell(sb: StringBuilder) {
        if (name == null)
            throw Exception("Cannot invoke global function")

        sb.append(mangledName)
    }
}
