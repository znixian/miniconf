package miniconf.exec

class Scope(val parent: Scope?) {
    val locals: MutableMap<String, VariableSlot> = HashMap()

    val uniqueId: Int = nextId++

    operator fun get(name: String): VariableSlot? {
        return locals[name] ?: parent?.get(name)
    }

    operator fun set(name: String, value: VariableSlot?) {
        if (value != null) {
            locals[name] = value
        } else {
            locals.remove(name)
        }
    }

    companion object {
        private var nextId: Int = 1
    }
}

class VariableSlot(val initialScope: Scope, val initialName: String) : Value() {
    val mangledName: String get() = "__miniconf_${initialName}_${initialScope.uniqueId}_$uniqueId"

    val uniqueId: Int = nextId++

    override fun toShell(sb: StringBuilder) {
        sb.append("\"\$$mangledName\"")
    }

    companion object {
        private var nextId: Int = 1
    }
}
