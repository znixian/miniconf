package miniconf.exec.commands

import miniconf.*
import miniconf.exec.AbstractRule
import miniconf.exec.FlagRule
import miniconf.exec.Rule
import miniconf.exec.RuleType
import miniconf.providers.GNUAssembler
import miniconf.providers.GNUCompiler
import miniconf.providers.GNUCompilerLinker
import kotlin.system.exitProcess

class Decl(var targetFile: String, var type: DeclTypes) : CommandValue() {
    val sources: MutableList<String> = ArrayList()
    val objects: MutableList<String> = ArrayList()
    val pkglibs: MutableList<String> = ArrayList()
    val flags: MutableMap<String, MutableList<String>> = HashMap()

    fun apply(rule: AbstractRule) {
        when (rule) {
            is Rule -> when (rule.type) {
                RuleType.COMPILE -> sources += rule.values
                RuleType.OBJECT -> objects += rule.values
                RuleType.PKGLIBS -> pkglibs += rule.values
            }
            is FlagRule -> {
                flags.computeIfAbsent(rule.target) { ArrayList() } += rule.values
            }
        }
    }

    fun produceRules(providers: List<IFileProvider>, linkers: List<ILinker>): List<NJBuildItem> {
        val items = ArrayList<NJBuildItem>()

        val sourceTypes = HashSet<String>()

        for (srcName in sources) {
            val src = RelativeFile(FileBase.SOURCE, srcName)

            var found = false

            for (provider in providers) {
                if (!provider.appliesTo(src))
                    continue

                sourceTypes += provider.id
                items += provider.generate(src, flags.getOrDefault(provider.id, listOf()), this)

                found = true
                break
            }

            if (!found) {
                System.err.println("Cannot process $srcName - no known handlers")
                exitProcess(2)
            }
        }

        val suitableLinkers = ArrayList<ILinker>()

        for (linker in linkers) {
            var compatible = true
            for (type in sourceTypes) {
                if (!linker.supports(type)) {
                    compatible = false
                    break
                }
            }

            if (!compatible)
                continue

            suitableLinkers += linker
        }

        // Pick the first linker we know of
        if (suitableLinkers.isEmpty()) {
            System.err.println("No suitable linkers for input types: ${sourceTypes.reduce { a, b -> "$a, $b" }}")
            exitProcess(1)
        }

        // TODO come up with a better way of handling this
        val linker = suitableLinkers.first()

        // Build a list of all the objects
        val objects = ArrayList<RelativeFile>()
        for (item in items)
            objects += item.outputs

        // Generate the link rule
        items += linker.generate(targetFile, objects, flags.getOrDefault("ld", listOf()), this)

        return items
    }

    override fun toShell(sb: StringBuilder) {
        // FIXME don't duplicate
        val providers = listOf(
                GNUCompiler("g++", "c++", listOf("cpp", "c++", "cxx", "cc")),
                GNUCompiler("gcc", "c", listOf("c")),
                GNUAssembler("as", "as", listOf("S"))
        )
        val linkers = listOf(
                GNUCompilerLinker("gcc", listOf("as", "c")),
                GNUCompilerLinker("g++", listOf("as", "c", "c++"))
        )
        val buildItems = produceRules(providers, linkers)
        ///

        val rules: Set<NJBuildRule> = buildItems.map { it.rule }.toSet()

        val ruleNames = HashMap<NJBuildRule, String>()

        // Print out the rules

        for (r in rules) {
            val name = ruleNames.computeIfAbsent(r) { "r${ruleNames.count()}" }

            val rstr = BuildString()
            rstr.add("rule $name\n")
            for (pair in r.properties) {
                rstr.add("  ")
                rstr.add(pair.key)
                rstr.pad()
                rstr.add("=")
                rstr.pad()
                val value = pair.value
                rstr.add(when(value) {
                    is BuildString -> value
                    is CommandBuilder -> BuildString(BuildString.Part.PrintCommand(value)) // TODO
                    else -> TODO()
                })
                rstr.add("\n")
            }

            sb.append("if check_rule $name; then\ncat << ___eof_mc_rule >> build.ninja\n")
            sb.append(rstr.toString(BuildString.Context.Configure).strip())
            sb.append("\n___eof_mc_rule\nfi\n")
        }

        sb.append("cat << ___eof_configure_ninjabuild >> build.ninja\n")
        for (i in buildItems) {
            val escapedOutputs = BuildString()
            for (s in i.outputs) {
                escapedOutputs.add(s)
                escapedOutputs.pad()
            }

            val escapedInputs = BuildString()
            for (s in i.inputs) {
                escapedInputs.add(s)
                escapedInputs.pad()
            }

            sb.appendln("build ${escapedOutputs.toString(BuildString.Context.Configure)}: ${ruleNames[i.rule]} ${escapedInputs.toString(BuildString.Context.Configure)}")
        }
        sb.append("___eof_configure_ninjabuild\n")
    }
}

enum class DeclTypes {
    UNSET,
    EXECUTABLE,
    SHARED_LIB,
    STATIC_LIB,
}