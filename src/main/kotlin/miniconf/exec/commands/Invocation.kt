package miniconf.exec.commands

import miniconf.exec.ScriptRunner
import miniconf.exec.Value

class Invocation(val context: ScriptRunner, val name: String, val args: List<Value>) : CommandValue() {
    override fun toShell(sb: StringBuilder) {
        val func = context.functions[name] ?: throw Exception("Unknown function $name")
        func.invokeAtShell(sb)

        for (arg in args) {
            sb.append(" ")
            arg.toShell(sb)
        }
        sb.append('\n')
    }
}
