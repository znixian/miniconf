package miniconf.exec.commands

import miniconf.exec.Value
import miniconf.exec.VariableSlot

class Assignment(val slot: VariableSlot, val expr: Value) : CommandValue() {
    override fun toShell(sb: StringBuilder) {
        sb.append(slot.mangledName)
        sb.append('=')
        expr.toShell(sb)
        sb.append('\n')
    }
}
