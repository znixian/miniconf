package miniconf.exec

import miniconf.IFunc
import miniconf.exec.commands.*
import xyz.znix.miniconf.antlr.MiniConfBaseVisitor
import xyz.znix.miniconf.antlr.MiniConfParser

class ScriptRunner : MiniConfBaseVisitor<Value>() {
    val globalFunction = ScriptFunc(null, listOf(), null)
    val functions: MutableMap<String, IFunc> = HashMap()

    val global get() = globalFunction.scope
    var scope = global

    init {
        functions["print"] = object : IFunc {
            override fun invokeAtShell(sb: StringBuilder) {
                sb.append("echo")
            }
        }
    }

    override fun visitProgram(ctx: MiniConfParser.ProgramContext): Value {
        for (block in ctx.block()) {
            when (val res = visit(block)) {
                ConstantValue.NULL -> Unit // Noop
                is CommandValue -> globalFunction.items += res
                else -> TODO("Unknown root result ${res.javaClass}")
            }
        }

        return ConstantValue.NULL
    }

    override fun visitDecl(ctx: MiniConfParser.DeclContext): Decl {
        val decl = Decl(visitFilename(ctx.filename()).text, DeclTypes.UNSET)

        for (rule in ctx.buildrule()) {
            val eval = visit(rule) as AbstractRule
            decl.apply(eval)
        }

        return decl
    }

    override fun visitTypeRule(ctx: MiniConfParser.TypeRuleContext): Rule {
        TODO()
    }

    override fun visitCompileRule(ctx: MiniConfParser.CompileRuleContext) = buildRule(RuleType.COMPILE, ctx.arg())
    override fun visitObjectRule(ctx: MiniConfParser.ObjectRuleContext) = buildRule(RuleType.OBJECT, ctx.arg())
    override fun visitPkgLibsRule(ctx: MiniConfParser.PkgLibsRuleContext) = buildRule(RuleType.PKGLIBS, ctx.arg())
    override fun visitFlagsRule(ctx: MiniConfParser.FlagsRuleContext) = FlagRule(ctx.ID().text, parseArg(ctx.arg()))

    private fun parseArg(arg: MiniConfParser.ArgContext): List<String> = when (arg) {
        is MiniConfParser.StringArgContext -> listOf((visit(arg.filename()) as StringValue).text)
        is MiniConfParser.ShortArgContext -> arg.filename().map { (visit(it) as StringValue).text }
        else -> TODO("Unknown arg ${arg.javaClass}")
    }

    private fun buildRule(type: RuleType, arg: MiniConfParser.ArgContext) = Rule(type, parseArg(arg))

    override fun visitFilename(ctx: MiniConfParser.FilenameContext): StringValue =
            ctx.quote()?.let { visitQuote(it) } ?: StringValue(ctx.text)

    override fun visitQuote(ctx: MiniConfParser.QuoteContext): StringValue {
        return StringValue(ctx.DOUBLE_QUOTE().text.let { it.substring(1, it.length - 1) })
    }

    override fun visitVariableRef(ctx: MiniConfParser.VariableRefContext): Value {
        val name = ctx.ID().text
        return scope[name] ?: throw Exception("Variable $name does not exist or is out of scope")
    }

    override fun visitDefineLocal(ctx: MiniConfParser.DefineLocalContext): Assignment {
        val name = ctx.ID().text
        if (scope.locals.containsKey(name))
            throw Exception("Cannot redefine variable $name at line ${ctx.start.line}")

        val slot = VariableSlot(scope, name)
        scope[name] = slot

        val expr = visit(ctx.expr())
        return Assignment(slot, expr)
    }

    override fun visitInvoke(ctx: MiniConfParser.InvokeContext): Invocation {
        val funcName = ctx.ID().text
        val args = ArrayList<Value>()

        for (expr in ctx.expr()) {
            args += visit(expr)
        }

        return Invocation(this, funcName, args)
    }

    override fun visitFunctionDefinition(ctx: MiniConfParser.FunctionDefinitionContext): Value {
        val args = ArrayList<String>()
        for (argname in ctx.args) {
            args += argname.text
        }

        val name = ctx.name.text
        val func = ScriptFunc(name, args, global)
        val oldScope = scope
        scope = func.scope

        functions[name] = func

        for (block in ctx.block()) {
            when (val v = visit(block)) {
                is CommandValue -> func.items += v
                else -> TODO()
            }
        }

        scope = oldScope
        return ConstantValue.NULL
    }
}