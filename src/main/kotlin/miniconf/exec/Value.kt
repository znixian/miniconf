package miniconf.exec

abstract class Value {
    abstract fun toShell(sb: StringBuilder)
}

class ConstantValue(val value: String) : Value() {
    override fun toShell(sb: StringBuilder) {
        sb.append(value)
    }

    companion object {
        val NULL = ConstantValue("")
        val TRUE = ConstantValue("true")
        val FALSE = ConstantValue("false")
    }
}

class StringValue(val text: String) : Value() {
    override fun toShell(sb: StringBuilder) {
        // TODO escape
        sb.append('"')
        sb.append(text)
        sb.append('"')
    }
}
