package miniconf.exec

sealed class AbstractRule(values: List<String>) : Value() {
    val values: List<String> = ArrayList(values)
}

class Rule(val type: RuleType, values: List<String>) : AbstractRule(values) {
    override fun toShell(sb: StringBuilder) {
        TODO("not implemented")
    }
}

class FlagRule(val target: String, values: List<String>) : AbstractRule(values) {
    override fun toShell(sb: StringBuilder) {
        TODO("not implemented")
    }
}

enum class RuleType {
    COMPILE,
    OBJECT,
    PKGLIBS,
}