package miniconf

import kotlin.streams.toList

class CommandBuilder {
    val words: MutableList<Word> = ArrayList()

    fun toNinja(context: BuildString.Context.Ninja): List<String> = words.stream().map {
        when (it) {
            is Word.Static -> it.value
            is Word.Builder -> it.value.toString(context)
        }
    }.toList()

    fun toShell(): String {
        val sb = StringBuilder()

        for (word in words) {
            if (sb.isNotEmpty())
                sb.append(' ')

            // TODO which mode?
            // TODO escaping
            when (word) {
                is Word.Static -> sb.append(word.value)
                is Word.Builder -> sb.append(word.value.toString(BuildString.Context.InlineShell))
            }
        }

        return sb.toString()
    }

    fun add(str: String) {
        words += Word.Static(str)
    }

    fun add(str: BuildString) {
        words += Word.Builder(str)
    }

    fun add(part: BuildString.Part) {
        add(BuildString(part))
    }

    fun addAll(strings: List<String>) {
        for (str in strings)
            add(str)
    }

    fun toString(mode: BuildString.Context): String {
        return when (mode) {
            is BuildString.Context.Ninja -> toNinja(mode).joinToString(" ")
            else -> TODO()
        }
    }

    sealed class Word {
        class Static(val value: String) : Word()
        class Builder(val value: BuildString) : Word()
    }
}
