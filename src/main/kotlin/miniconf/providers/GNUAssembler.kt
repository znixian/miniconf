package miniconf.providers

import miniconf.*
import miniconf.exec.commands.Decl

class GNUAssembler(val name: String, override val id: String, val filenames: List<String>) : IFileProvider {
    private val rulePool: MutableMap<String, NJBuildRule> = HashMap()

    override fun appliesTo(file: RelativeFile): Boolean {
        return filenames.any { file.name.endsWith(it) }
    }

    override fun generate(file: RelativeFile, flags: List<String>, decl: Decl): NJBuildItem {
        val objName = file.nameWithoutExtension + ".o"

        val cmdLine = BuildString()
        cmdLine.addBashArgument(name)
        cmdLine.pad()
        cmdLine.add("\$in")
        cmdLine.addBashArgument("-o")
        cmdLine.pad()
        cmdLine.add("\$out")

        for (flag in flags)
            cmdLine.addBashArgument(flag)

        val props = HashMap<BuildString, BuildString>()
        props[BuildString("command")] = cmdLine

        val rule = rulePool.computeIfAbsent(cmdLine.toString()) { NJBuildRule(props) }

        return NJBuildItem(rule, listOf(RelativeFile(FileBase.OBJECTS, objName)), listOf(file))
    }
}
