package miniconf.providers

import miniconf.*
import miniconf.exec.commands.Decl

class GNUCompiler(val name: String, override val id: String, val filenames: List<String>) : IFileProvider {
    private val rulePool: MutableMap<String, NJBuildRule> = HashMap()

    override fun appliesTo(file: RelativeFile): Boolean {
        return filenames.any { file.name.endsWith(it) }
    }

    override fun generate(file: RelativeFile, flags: List<String>, decl: Decl): NJBuildItem {
        val objName = file.nameWithoutExtension + ".o"

        val cmdLine = CommandBuilder()
        cmdLine.add(name)
        cmdLine.add("-c")
        cmdLine.add("\$in")
        cmdLine.add("-o")
        cmdLine.add("\$out")
        cmdLine.add("-MMD")
        cmdLine.add("-MF")
        cmdLine.add("\$out.d")

        // TODO handle splitting?
        for (flag in flags)
            cmdLine.add(flag)

        // Add pkgconfig libraries
        if (decl.pkglibs.size > 0) {
            val args = CommandBuilder()
            args.add("pkg-config")
            args.add("--cflags")
            args.addAll(decl.pkglibs)

            // TODO handle splitting
            cmdLine.add(BuildString.Part.ShellEval(args))
        }

        val props = HashMap<BuildString, Any>()
        props[BuildString("depfile")] = BuildString("${'$'}out.d")
        props[BuildString("command")] = cmdLine

        val rule = rulePool.computeIfAbsent(cmdLine.toString()) { NJBuildRule(props) }

        return NJBuildItem(rule, listOf(RelativeFile(FileBase.OBJECTS, objName)), listOf(file))
    }
}
