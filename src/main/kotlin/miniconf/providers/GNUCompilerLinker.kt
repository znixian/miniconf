package miniconf.providers

import miniconf.*
import miniconf.exec.commands.Decl

class GNUCompilerLinker(val name: String, val supported: List<String>) : ILinker {
    private val rules = HashMap<String, NJBuildRule>()

    override fun supports(id: String): Boolean = supported.contains(id)

    override fun generate(output: String, inputs: List<RelativeFile>, flags: List<String>, decl: Decl): NJBuildItem {
        val cmdline = CommandBuilder()

        cmdline.add(name)
        cmdline.add("-o")
        cmdline.add("\$out")
        cmdline.add("\$in")

        cmdline.addAll(flags)

        // Add pkgconfig libraries
        if (decl.pkglibs.size > 0) {
            val args = CommandBuilder()
            args.add("pkg-config")
            args.add("--libs")
            args.addAll(decl.pkglibs)

            // Trust it won't produce any special output
            // cmdline.add(processToString(args))
            cmdline.add(BuildString.Part.ShellEval(args))
        }

        val rule = rules.computeIfAbsent(cmdline.toString()) {
            NJBuildRule(mapOf(Pair(BuildString("command"), cmdline)))
        }

        return NJBuildItem(rule, listOf(RelativeFile(FileBase.BUILD, output)), inputs)
    }
}