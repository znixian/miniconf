grammar MiniConf;

@lexer::members {
    int lastTokenType = 0;
    @Override
    public void emit(Token token) {
        super.emit(token);
        lastTokenType = token.getType();
    }
}

ID : [a-z_]+ ;             // match lower-case identifiers
WS : [ \t\r]+ -> skip ; // skip spaces, tabs, newlines
LINE_COMMENT  : '#' ~[\n]* -> skip ;
FILENAME : [a-zA-Z0-9\-_.+]+ ; // TODO unicode?
DOUBLE_QUOTE: '"' ('\\"'|.)*? '"';
EQ : '=';
BKR_S : ')'; // Bracket right soft
BKR_H : ']'; // Bracket right hard
BKR_C : '}'; // Bracket right curly
BKL_S : '('; // Bracket left soft
BKL_H : '['; // Bracket left hard
BKL_C : '{'; // Bracket left curly
EOL: '\n' {
    // Ignore newlines if they come after a token that cannot finish a statement
    switch(lastTokenType) {
    // These can finish statements
    case ID:
    case DOUBLE_QUOTE:
    case BKR_S: case BKR_H: case BKR_C:
    case BKL_S: case BKL_H: case BKL_C:
        break;
    // The others cannot
    case EOL:
    default:
        skip();
    }
};

program: (EOL | block EOL)* block? EOF;
block:
    filename ':' 'link' BKL_C EOL (EOL | buildrule EOL)* BKR_C # decl
    | ID '.' buildrule # decl_mod
    | 'local' ID EQ expr # DefineLocal
    | ID '(' ((expr ',')* expr)? ')' # Invoke
    | 'function' ':' name=ID '(' ((args+=ID ',')* args+=ID)? ')' EOL? '{' EOL (EOL | block EOL)* '}' # FunctionDefinition
;

buildrule:
    'type' ID # TypeRule
    | 'compile' arg # CompileRule
    | 'object' arg # ObjectRule
    | 'pkglibs' arg # PkgLibsRule
    | 'flags' ID arg # FlagsRule
;

filename: FILENAME | ID | quote;

arg:
    filename # StringArg
    | BKL_S filename* BKR_S # ShortArg
    | BKL_H expr* BKR_H # LongArg
;

quote: DOUBLE_QUOTE;

expr:
    ID # VariableRef
    | quote # ArgExpr
;
